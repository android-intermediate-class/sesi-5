package edu.intermediate.sesi5.data

sealed class Response<out T : Any>

data object Loading : Response<Nothing>()
class Success<out T : Any>(val data: T) : Response<T>()
class Failed(val error: Error) : Response<Nothing>()