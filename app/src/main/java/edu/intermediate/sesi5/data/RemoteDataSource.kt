package edu.intermediate.sesi5.data

import edu.intermediate.sesi5.api.ApiService
import edu.intermediate.sesi5.data.model.ArticleResponse

class RemoteDataSource(
    private val apiService: ApiService
) {

    suspend fun getHeadlines(page: Int): ArticleResponse = apiService.getHeadlines(page = page)

    suspend fun getEverything(query: String, page: Int): ArticleResponse {
        return apiService.getEverything(query = query, page = page)
    }
}