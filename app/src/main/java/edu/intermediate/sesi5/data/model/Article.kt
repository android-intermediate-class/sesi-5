package edu.intermediate.sesi5.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ArticleResponse(
    @SerialName("articles") val articles: List<Article>,
    @SerialName("status") val status: String,
    @SerialName("totalResults") val totalResults: Int
)

@Serializable
@Parcelize
data class Article(
    @SerialName("author") val author: String? = null,
    @SerialName("content") val content: String? = null,
    @SerialName("description") val description: String? = null,
    @SerialName("publishedAt") val publishedDate: String? = null,
    @SerialName("source") val source: Source? = null,
    @SerialName("title") val title: String? = null,
    @SerialName("url") val url: String? = null,
    @SerialName("urlToImage") val imageUrl: String? = null
) : Parcelable