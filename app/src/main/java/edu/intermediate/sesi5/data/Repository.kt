package edu.intermediate.sesi5.data

import edu.intermediate.sesi5.data.model.ArticleResponse

class Repository(
    private val remoteDataSource: RemoteDataSource
) {

    suspend fun getHeadlines(page: Int): ArticleResponse = remoteDataSource.getHeadlines(page)

    suspend fun getEverything(query: String, page: Int): ArticleResponse {
        return remoteDataSource.getEverything(query, page)
    }
}