package edu.intermediate.sesi5.data

class Error(
    val title: String,
    val message: String
)