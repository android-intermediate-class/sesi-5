package edu.intermediate.sesi5.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import edu.intermediate.sesi5.BuildConfig
import edu.intermediate.sesi5.data.Error
import edu.intermediate.sesi5.data.Failed
import edu.intermediate.sesi5.data.Loading
import edu.intermediate.sesi5.data.Repository
import edu.intermediate.sesi5.data.Response
import edu.intermediate.sesi5.data.Success
import edu.intermediate.sesi5.data.model.ArticleResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class MainViewModel(
    private val repository: Repository
) : ViewModel(), CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private var _topHeadlines = MutableLiveData<Response<ArticleResponse>>()
    val topHeadlines: LiveData<Response<ArticleResponse>>
        get() = _topHeadlines

    private var _articleSearchResult = MutableLiveData<Response<ArticleResponse>>()
    val articleSearchResult: LiveData<Response<ArticleResponse>>
        get() = _articleSearchResult

    fun getHeadlines(page: Int) {
        _topHeadlines.value = Loading
        launch {
            try {
                val articles = withContext(Dispatchers.IO) {
                    repository.getHeadlines(page)
                }
                _topHeadlines.value = Success(articles)
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                _topHeadlines.value = Failed(Error("error", "terjadi kesalahan"))
            }
        }
    }

    fun searchArticles(query: String?, page: Int) {
        _articleSearchResult.value = Loading
        launch {
            try {
                val articles = withContext(Dispatchers.IO) {
                    repository.getEverything(query ?: "", page)
                }
                _articleSearchResult.value = Success(articles)
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                _articleSearchResult.value = Failed(Error("error", "terjadi kesalahan"))
            }
        }
    }
}