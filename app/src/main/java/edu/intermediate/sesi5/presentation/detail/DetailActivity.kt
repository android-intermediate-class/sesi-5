package edu.intermediate.sesi5.presentation.detail

import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import edu.intermediate.sesi5.R
import edu.intermediate.sesi5.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {

    private var _layout: ActivityDetailBinding? = null

    private val layout: ActivityDetailBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityDetailBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        val articleUrl = intent?.extras?.getString(ARTICLE_URL, "") ?: ""

        setSupportActionBar(layout.toolbar.root)
        supportActionBar?.apply {
            title = getString(R.string.detail_title)
            setDisplayHomeAsUpEnabled(true)
        }

        layout.webView.apply {
            settings.apply {
                javaScriptEnabled = true
                loadWithOverviewMode = true
                loadsImagesAutomatically = true
            }

            webViewClient = WebViewClient() // for preventing opening browser
        }

        if (articleUrl.isEmpty()) {
            layout.containerError.isVisible = true
            layout.webView.isVisible = false
            layout.tvErrorTitle.text = getString(R.string.detail_error_title)
        } else {
            layout.containerError.isVisible = false
            layout.webView.isVisible = true
            layout.webView.loadUrl(articleUrl)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }

    companion object {
        const val ARTICLE_URL = "article_url"
    }
}