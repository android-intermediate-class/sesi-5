package edu.intermediate.sesi5.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import edu.intermediate.sesi5.R
import edu.intermediate.sesi5.data.model.Article
import edu.intermediate.sesi5.databinding.ItemNewsBinding
import edu.intermediate.sesi5.util.formattedDate
import java.text.SimpleDateFormat
import java.util.Locale

class NewsAdapter(
    private val mArticles: MutableList<Article>,
    private val itemClick: (String) -> Unit
) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val binding = ItemNewsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return NewsViewHolder(binding)
    }

    fun setData(articles: List<Article>) {
        mArticles.clear()
        mArticles.addAll(articles)
        notifyDataSetChanged()
    }

    fun updateDate(articles: List<Article>) {
        mArticles.addAll(articles)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(mArticles[position])
    }

    override fun getItemCount(): Int = mArticles.size

    inner class NewsViewHolder(
        val binding: ItemNewsBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(article: Article) {
            binding.root.setOnClickListener { itemClick(article.url ?: "") }
            Picasso.get()
                .load(article.imageUrl)
                .error(R.drawable.broken_image)
                .into(binding.ivItemNews)
            binding.tvItemNewsTitle.text = article.title
            binding.tvItemNewsDate.text = article.publishedDate?.formattedDate()
        }
    }
}