package edu.intermediate.sesi5.presentation

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import edu.intermediate.sesi5.R
import edu.intermediate.sesi5.data.Failed
import edu.intermediate.sesi5.data.Loading
import edu.intermediate.sesi5.data.Repository
import edu.intermediate.sesi5.data.Success
import edu.intermediate.sesi5.databinding.ActivityMainBinding
import edu.intermediate.sesi5.factory.MainViewModelFactory
import edu.intermediate.sesi5.presentation.detail.DetailActivity
import edu.intermediate.sesi5.util.Util

class MainActivity : AppCompatActivity() {

    private var _layout: ActivityMainBinding? = null
    private val layout: ActivityMainBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    private lateinit var mViewModel: MainViewModel
    private var mAdapter: NewsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        setSupportActionBar(layout.toolbar.root)
        supportActionBar?.title = getString(R.string.app_name)

        mViewModel = ViewModelProvider(
            this,
            MainViewModelFactory(Repository(Util.remoteDataSource))
        )[MainViewModel::class.java]

        mViewModel.topHeadlines.observe(this) { result ->
            when (result) {
                is Loading -> {
                    layout.loading.isVisible = true
                    layout.rvArticles.isVisible = false
                    layout.containerError.isVisible = false
                }
                is Success -> {
                    mAdapter?.setData(result.data.articles)
                    layout.loading.isVisible = false
                    layout.rvArticles.isVisible = true
                    layout.containerError.isVisible = false
                }
                is Failed -> {
                    layout.loading.isVisible = false
                    layout.rvArticles.isVisible = false
                    layout.containerError.isVisible = true
                    layout.tvErrorTitle.text = result.error.title
                }
            }
        }

        mViewModel.articleSearchResult.observe(this) { result ->
            when (result) {
                is Loading -> {
                    layout.loading.isVisible = true
                    layout.rvArticles.isVisible = false
                    layout.containerError.isVisible = false
                }
                is Success -> {
                    mAdapter?.setData(result.data.articles)
                    layout.loading.isVisible = false
                    layout.rvArticles.isVisible = true
                    layout.containerError.isVisible = false
                }
                is Failed -> {
                    layout.loading.isVisible = false
                    layout.rvArticles.isVisible = false
                    layout.containerError.isVisible = true
                    layout.tvErrorTitle.text = result.error.title
                }
            }
        }

        mAdapter = NewsAdapter(mutableListOf()) { articleUrl ->
            val intent = Intent(this, DetailActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                putExtra(DetailActivity.ARTICLE_URL, articleUrl)
            }
            startActivity(intent)
        }

        layout.rvArticles.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = mAdapter
        }

        mViewModel.getHeadlines(1)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val searchViewItem = menu?.findItem(R.id.menu_search)
        val searchView = searchViewItem?.actionView as SearchView
        val editText = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        editText.setTextColor(getColor(R.color.colorAccent))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                mViewModel.searchArticles(query, 1)

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
        searchView.setOnCloseListener {
            mViewModel.getHeadlines(1)
            false
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }
}
