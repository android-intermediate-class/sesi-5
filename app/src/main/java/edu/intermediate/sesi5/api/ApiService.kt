package edu.intermediate.sesi5.api

import edu.intermediate.sesi5.data.model.ArticleResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("top-headlines")
    suspend fun getHeadlines(
        @Query("country") country: String? = "id",
        @Query("pageSize") pageSize: Int = 10,
        @Query("page") page: Int = 1
    ): ArticleResponse

    @GET("everything")
    suspend fun getEverything(
        @Query("q") query: String,
        @Query("pageSize") pageSize: Int = 10,
        @Query("page") page: Int = 1
    ): ArticleResponse
}