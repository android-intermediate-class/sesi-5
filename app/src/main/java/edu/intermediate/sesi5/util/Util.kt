package edu.intermediate.sesi5.util

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import edu.intermediate.sesi5.BuildConfig
import edu.intermediate.sesi5.api.ApiService
import edu.intermediate.sesi5.data.RemoteDataSource
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

object Util {

    fun okhttp(apiKey: String): OkHttpClient {
        return OkHttpClient.Builder().apply {
            addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            addInterceptor { chain ->
                chain.proceed(chain.let {
                    it.request().newBuilder()
                        .header("X-Api-Key", apiKey)
                        .method(it.request().method, it.request().body)
                        .build()
                })
            }
        }.build()
    }

    fun apiService(okHttpClient: OkHttpClient): ApiService {
        val contentType = "application/json".toMediaType()
        val json = Json {
            ignoreUnknownKeys = true
        }.asConverterFactory(contentType)
        val retrofit = Retrofit.Builder().apply {
            client(okHttpClient)
            baseUrl(BuildConfig.BASE_URL)
            addConverterFactory(json)
        }.build()

        return retrofit.create(ApiService::class.java)
    }

    val remoteDataSource = RemoteDataSource(apiService(okhttp(BuildConfig.API_KEY)))
}

fun String?.formattedDate(): String {
    if (this == "" || this == null) return ""

    val inputDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    val outputDateFormat = SimpleDateFormat("dd MMM yyyy HH:mm", Locale("id", "ID"))
    val date = inputDateFormat.parse(this) ?: Date()

    return outputDateFormat.format(date)
}